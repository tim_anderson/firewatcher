'''
Created on Jun 24, 2013

@author: tim.anderson

    Menu for the Firewatcher application.

'''
from Tkinter import Menu
from tools.ConfigValues import ProgramTitle
class FirewatcherMenu (Menu):
    def __init__(self, parent, runCommand, videoCommand, aboutCommand, exitCommand, **options):
        Menu.__init__(self, parent, **options)
        _fileMenu = Menu(self, tearoff = 0)
        _fileMenu.add_command(label = "Run", command = runCommand, accelerator = "Ctrl+A")
        _fileMenu.add_command(label = "Video options", command = videoCommand, accelerator = "Ctrl+O", state = "disabled")
        _fileMenu.add_command(label = "About " + ProgramTitle, command = aboutCommand, accelerator = "Ctrl+B")
        _fileMenu.add_cascade(label = "Exit", command = exitCommand, accelerator = "Ctrl+Q")
        self.add_cascade(label = "File", menu = _fileMenu)
        
        parent.bind_all("<Control-a>", runCommand)
        parent.bind_all("<Control-o>", videoCommand)
        parent.bind_all("<Control-b>", aboutCommand)
        parent.bind_all("<Control-q>", exitCommand)
