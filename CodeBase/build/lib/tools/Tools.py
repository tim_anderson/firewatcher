'''
Created on Jun 10, 2013

@author: tim.anderson
'''
from datetime import datetime, timedelta
from os import path, listdir
from os.path import exists
from subprocess import call, Popen
from sys import platform
import ConfigValues
import ImgHelper
from Processing import CreateTimestamp
from Regex import getHourMinuteWithRegex, getRegexListFromFile, getMatchingRegexes
import Image


# creates an avi of the given images, and returns the filename.
def CreateVideoFile(params):
    filename = params["Filename"]
    fps = params["Videofps"] if params["Videofps"] != "0" else "1"
    imageNamePattern = path.join(ConfigValues.tempDirectory, ConfigValues.ImagePattern)
    call([ConfigValues.FFMpegLocation,
            "-y",
            "-f",
            "image2",
            "-r",
            fps,
            "-i",
            imageNamePattern,
            "-c:v",
            ConfigValues.encoder,
            "-r",
            fps,
            path.join(ConfigValues.outputDirName, filename),
            ])

def CreateTempDirIfMissing():
    from os import mkdir
    if not exists(ConfigValues.tempDirectory):
        mkdir(ConfigValues.tempDirectory)

def getImagesFromServer(imageList, progressFunct, addTimestamps):

#     try:
    numberOfImages = len(imageList)
    x = 0

    numberOfIgnoredImages = 0
    for image in imageList:
        if image[0] == "ignore":
            numberOfIgnoredImages += 1
        else:
            digits = len(str(x))
            extraZerosNeeded = 5 - digits
            filename = path.abspath(path.join(ConfigValues.tempDirectory,
                                    "".join(["Image", "0"*extraZerosNeeded, str(x), ".jpeg"])))
            imageSuccess = DownloadImage(image[0], filename, image[1], addTimestamps)
            if imageSuccess == "success":
                x += 1
                progressFunct("Download of image " + str(x + numberOfIgnoredImages) + " / " + str(numberOfImages))
            elif imageSuccess == "TimestampFail":
                x += 1
                progressFunct("img" + str(x + numberOfIgnoredImages) + "/" + str(numberOfImages) + ", but could not timestamp.")
            else:

                progressFunct("downlodading " + image[0] + " was not successful.")


#     except:
#         progressFunct("Runtime error in download,")

def DownloadImage(inFilename, outFilename, datetime, timestampImage):
#     try:
        img = Image.open(inFilename)
        if timestampImage == "1":
            fontImage = CreateTimestamp(datetime)
            if fontImage == None:
                return "TimestampFail"
            img.paste(fontImage)
        img.save(outFilename)
        return "success"
#     except Exception:
#         import logging
#         logging.basicConfig(leve = logging.DEBUG, filename = "errorlog.txt")
#         logging.exception("msg")
#         return "fail"


def GetImageList(params, progressFunct):
    camDir = path.join(params["SelectedFolder"], params["CamFolder"])
    if exists(camDir):
        subfolders = []
        folderList = listdir(camDir)
        for folder in folderList:
            fullPath = path.join(params["SelectedFolder"], params["CamFolder"], folder)
            if path.isdir(fullPath):
                subfolders.append(fullPath)
        subfolders.sort
        imageList = []
        for folder in subfolders:
            foundImages = __addImagesFromFolder(folder, params["StartDateTime"], params["EndDateTime"], progressFunct)
            imageList.extend(foundImages)
            progressFunct(str(len(foundImages)) + " images found...")
        return imageList
    else:
        return []

def __addImagesFromFolder(folder, startDateTime, endDateTime, progressFunct):
    imageList = []
    lastImageTime = datetime(year = 2000, month = 1, day = 1)  # base datetime below what's going to be used.
    capTimespan = timedelta(minutes = ConfigValues.minutesPerFrameCap)
    if folder[-8::].isdigit():
        year = int(folder[-8:-4])
        month = int(folder[-4:-2])
        day = int(folder[-2::])
        folderDate = datetime(year = year, month = month, day = day)
        # check if folder date is between the start date and the end date.
        if (startDateTime.date()) <= folderDate.date() <= endDateTime.date():
            images = [x for x in listdir(folder) if path.isfile(path.join(folder, x))]
            for image in images:
                imageHour, imageMinute = getHourMinuteWithRegex(image)
                imageDateTime = datetime(folderDate.year, folderDate.month, folderDate.day, imageHour, imageMinute)
                if(startDateTime <= imageDateTime <= endDateTime):
                    if (not ConfigValues.capFramerate) or (imageDateTime - capTimespan > lastImageTime):
                        imageList.append([path.join(folder, image), imageDateTime ])
                        lastImageTime = imageDateTime
                elif imageDateTime > endDateTime:
                    break  # stop looking through the folders if you've already passed the end time.
    return imageList

def OpenInExplorer(fileLoc):
    if platform == "win32":
        Popen(r'explorer /select, "' + path.abspath(fileLoc) + r'"')
    elif platform == "darwin":
        Popen(['open', '-R', path.abspath(fileLoc)])

def findCorrectRegex(imageList):
    getMatchingRegexes(imageList[0])

def GenerateVideo (updateFunct, progressFunct, finishFunct, params):
    CreateTempDirIfMissing()
    ImgHelper.CleanupTempFiles()
    getRegexListFromFile()
    updateFunct ("Creating list of files to grab...")
    imageList = GetImageList(params, progressFunct)
    if len(imageList) == 0:  # quit out if video would have no frames in it.
        updateFunct("Video was not generated. No images were found in the given timespan. Did you select the wrong start date, or give an non-existant folder?")
        finishFunct()
        return
    ImgHelper.sortImageList(imageList)
    if params["IgnoreState"] == True:
        updateFunct ("Removing ignored images...")
        ImgHelper.labelIgnoredImages(imageList, params)
    updateFunct ("Downloading the images from " + params["CamFolder"] + "...")
    getImagesFromServer(imageList, progressFunct, params["UseTimestamps"])
    updateFunct ("Generating video...")
    CreateVideoFile(params)
    updateFunct ("Cleaning up temp files...")
    ImgHelper.CleanupTempFiles()
    updateFunct ("Finished generating video. Video should be located at: ")
    updateFunct("Rendered_Videos\\" + params["Filename"] + ".")
    OpenInExplorer(path.abspath(path.join(ConfigValues.outputDirName, params["Filename"])))
    finishFunct ()

def GenerateVideoNoOutput(**params):
    ImgHelper.CleanupTempFiles()
    getRegexListFromFile()
    imageList = GetImageList(params, progressFunct = None)
    if len(imageList) != 0:
        ImgHelper.sortImageList(imageList)
        ImgHelper.labelIgnoredImages(imageList, params)
        getImagesFromServer(imageList, None)
        CreateVideoFile(params)
        ImgHelper.CleanupTempFiles()
