'''
Created on Jun 3, 2013

@author: tim.anderson

    Class acting as the base of the Gui for the Firewatcher application.
    It inherits from the main Tk root of Tkinter, and is composed of megawidgest
    from the rest of the Gui classes.

'''

from tools.ConfigValues import fileExtention
from tools.Tools import GenerateVideo
from FirewatcherMenu import FirewatcherMenu
from IgnoreTimePanel import IgnoreTimePanel
from ProgramOptionsFrame import ProgramOptionsFrame
from DateSelectionFrame import DateSelectionFrame
from DisplayFrame import DisplayFrame
from Popups import AboutScreen, nameChoicePopup
from thread import start_new_thread
from threading import Thread
from Tkinter import Tk
from datetime import timedelta
from sys import exit as progExit

class Gui(Tk):
    def __init__(self):
        Tk.__init__(self)        
        self.rowconfigure(index = 1, weight = 1)        # allows the ignorePanel to expand to it's correct size.
        self.columnconfigure(index = 1, weight = 1)
        self.ignorePanel = IgnoreTimePanel(self, text = "Hours to Ignore")
        self.ignorePanel.grid(row = 1, column = 0, sticky = "news")
        self.displayFrame = DisplayFrame(self, analyzeCommand = self.__Analyze,text = "Output:")
        self.displayFrame.grid(row = 3, column = 0, columnspan = 4, sticky = "news", padx = "4", pady = "4")
        self.dateFrame = DateSelectionFrame(self)
        self.dateFrame.grid(row = 0, column = 0, sticky = "nesw", ipady = "4")
        
        self.optionsFrame = ProgramOptionsFrame(self, text = "Program Options")
        self.optionsFrame.grid(row = 0, column = 3, rowspan = "2", sticky = "news", padx = "4", ipadx = "4")
        mainmenu = FirewatcherMenu(
                                   self,
                                   runCommand = self.__Analyze,
                                   videoCommand = None,
                                   aboutCommand = AboutScreen,
                                   exitCommand = progExit)
        self.config(menu = mainmenu)
        
    def __Run(self, filename):
        params = {}
        params["Filename"] = filename
        params["SelectedFolder"] = self.optionsFrame.getSelectedFolder()
        params["CamFolder"] = self.optionsFrame.getCamFolder()
        params["StartDateTime"] = self.dateFrame.getStartDateTime()
        params["EndDateTime"] = params["StartDateTime"] + self.dateFrame.getRunTime()
        params["IgnoreStart"] = self.ignorePanel.GetStartTime()
        params["IgnoreEnd"] = self.ignorePanel.GetEndTime()
        params["IgnoreState"] = self.ignorePanel.GetIgnoreState()
        params["Videofps"] = self.optionsFrame.getVideoFPS()
        params["UseTimestamps"] = self.optionsFrame.GetTimestampChoice()
        self.displayFrame.IsRunButtonEnabled(False)
        self.displayFrame.appendLinesAtEnd(
                        ["---------------------",
                         "---------------------",
                         "Beginning Video Creation. This may take some time, so please be patient."])
        start_new_thread(GenerateVideo, (
                                self.displayFrame.appendAtEnd,
                                self.displayFrame.replaceFirstLine,
                                lambda : self.displayFrame.IsRunButtonEnabled(True),
                                params
                                ))
        Thread.start

    def ShowOutput(self, strings):
        for string in strings:
            self.displayFrame.appendAtEnd(string)
    def __CreateNamingPopup(self):
        nameChoicePopup(
                        "-".join([self.optionsFrame.getCamFolder(),self.dateFrame.getStartDateTime().date().isoformat(),fileExtention]), 
                        lambda filename:self.__Run(filename))

    # Checks the parameters. If they are valid, then runs the ffmpeg tool.
    def __Analyze(self):
        success = True
        self.displayFrame.clearOutput()
        if self.dateFrame.getStartDateTime() == None:
            success = False
            self.displayFrame.appendAtEnd("Warning: No start date is currently selected.")
        if self.optionsFrame.getSelectedFolder() is None:
            success = False
            self.displayFrame.appendAtEnd("Warning: No camera folder was selected from the list above.")
        if self.dateFrame.getRunTime() == timedelta():      #compares to a timedelta of no time change.
            success = False
            self.displayFrame.appendAtEnd("Warning: length of video is currently set to 0 days, 0 hours, 0 minutes.")
            self.displayFrame.appendAtEnd("Change length to generate video.")
        if success is True:
            self.__CreateNamingPopup()
