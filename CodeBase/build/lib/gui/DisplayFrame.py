'''
Created on Jun 24, 2013

@author: tim.anderson

    Megawidget containing the output listbox, as well as the run button.
    Use IsRunButtonEnabled to enable or disable the run functionality.

'''
from Tkinter import LabelFrame, Listbox, Frame, Button
class DisplayFrame(LabelFrame):
    def __init__(self, parent, analyzeCommand, **options):
        LabelFrame.__init__(self, parent, **options)
        self.displayListbox = Listbox(self)

        self.displayListbox.pack(fill = "both", expand = 1, padx = "4", pady = "4", side = "left")
        buttonFrame = Frame(self)
        buttonFrame.pack(anchor = "e", side = "right", fill = "both", padx = 4)
        self.runButton = Button(buttonFrame, command = analyzeCommand, text = "Generate Video", height = 2, width = 14)
        self.runButton.pack(anchor = "ne", side = "top", pady = 8)

    def clearOutput(self):
        self.displayListbox.delete(0, "end")

    def replaceFirstLine(self, text):
        self.displayListbox.insert(0, text)
        self.displayListbox.delete(1)

    def appendAtEnd(self, text):
        self.displayListbox.insert("end", text)

    def appendLinesAtEnd(self, lines):
        insert = self.displayListbox.insert
        map(lambda line: insert("end", line), lines)

    # sets the state of the run button, useful for turning off the button when already running.
    # options are "normal" and "disabled"
    def IsRunButtonEnabled(self, buttonState):
        if buttonState == True:
            self.runButton.config(state = "normal")  
        else: 
            self.runButton.config(state = "disabled")
