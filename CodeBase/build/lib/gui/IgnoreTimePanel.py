'''
Created on Jun 24, 2013

@author: tim.anderson

    Megawidget for the ignore times panel.
    use GetStartTime() and GetIgnoreTime() to return time objects for each.
    use GetIgnoreState() for whether the user wants to ignore those times or not.

'''
from Tkinter import Frame, LabelFrame, OptionMenu, Label, Checkbutton, StringVar
from ttk import Separator
from tools.ConfigValues import hoursList, minutesList, DefaultValues
from datetime import time


class IgnoreTimePanel(LabelFrame):
    def __init__(self, parent, **options):
        LabelFrame.__init__(self, parent, **options)
         
        defaultIgnoreState = DefaultValues["IgnoreState"]
        self.ignoreHoursStateVar        = StringVar()
        self.ignoreStartMinuteVar       = StringVar()
        self.ignoreStartHourVar         = StringVar()
        self.ignoreStartTimeOfDayVar    = StringVar()
        self.ignoreEndMinuteVar         = StringVar()
        self.ignoreEndHourVar           = StringVar()
        self.ignoreEndTimeOfDayVar      = StringVar()
        self.ignoreHoursStateVar        .set(defaultIgnoreState)
        self.ignoreStartMinuteVar       .set(DefaultValues["IgnoreStartMinute"])
        self.ignoreStartHourVar         .set(DefaultValues["IgnoreStartHour"])
        self.ignoreStartTimeOfDayVar    .set(DefaultValues["IgnoreStartTimeOfDay"])
        self.ignoreEndMinuteVar         .set(DefaultValues["IgnoreEndMinute"])
        self.ignoreEndHourVar           .set(DefaultValues["IgnoreEndHour"])
        self.ignoreEndTimeOfDayVar      .set(DefaultValues["IgnoreEndTimeOfDay"])
         
 
        ignoreHoursToggleFrame = Frame(self)
        ignoreHoursToggleFrame.pack(side = "top", fill = "both", expand = 1)
        self.ignoreHoursInputFrame = Frame(self)
        timeLabelFrame = Frame(self.ignoreHoursInputFrame)
        timeLabelFrame.pack(side = "top", fill = "both")
        ignoreHoursToggle = Checkbutton(ignoreHoursToggleFrame, text = "Ignore parts of the day?",
                                    command = lambda : self.__ApplyIgnoreHoursState(), pady = 4,
                                    variable = self.ignoreHoursStateVar, onvalue = "normal", offvalue = "disabled")
        ignoreHoursToggle.pack()
        self.ignoreStartTimeHour = OptionMenu(self.ignoreHoursInputFrame, self.ignoreStartHourVar, *hoursList)
        self.ignoreStartTimeHour.config(state = defaultIgnoreState, width = "2")
        self.ignoreStartTimeHour.pack(anchor = "w", side = "left")
        seperator1 = Label(self.ignoreHoursInputFrame, text = ":")
        seperator1.pack(anchor = "w", side = "left", padx = "0")
        self.ignoreStartTimeMinute = OptionMenu(self.ignoreHoursInputFrame, self.ignoreStartMinuteVar, *minutesList)
        self.ignoreStartTimeMinute.config(state = defaultIgnoreState)
        self.ignoreStartTimeMinute.pack(anchor = "w", side = "left")
        self.ignoreStartTimeOfDay = OptionMenu(self.ignoreHoursInputFrame, self.ignoreStartTimeOfDayVar, "AM", "PM")
        self.ignoreStartTimeOfDay.config(state = defaultIgnoreState, width = "2")
        self.ignoreStartTimeOfDay.pack(anchor = "w", side = "left", padx = "8")
        spacer = Separator(self.ignoreHoursInputFrame, orient = "horizontal")
        spacer.pack(padx = "20")
        self.ignoreEndTimeOfDay = OptionMenu(self.ignoreHoursInputFrame, self.ignoreEndTimeOfDayVar, "AM", "PM")
        self.ignoreEndTimeOfDay.config(state = defaultIgnoreState, width = "2")
        self.ignoreEndTimeOfDay.pack(anchor = "e", side = "right")
        self.ignoreEndTimeMinute = OptionMenu(self.ignoreHoursInputFrame, self.ignoreEndMinuteVar, *minutesList)
        self.ignoreEndTimeMinute.config(state = defaultIgnoreState)
        self.ignoreEndTimeMinute.pack(anchor = "e", side = "right")
        seperator2 = Label(self.ignoreHoursInputFrame, text = ":")
        seperator2.pack(anchor = "e", side = "right")
        self.ignoreEndTimeHour = OptionMenu(self.ignoreHoursInputFrame, self.ignoreEndHourVar, *hoursList)
        self.ignoreEndTimeHour.config(state = defaultIgnoreState, width = "2")
        self.ignoreEndTimeHour.pack(anchor = "e", side = "right")
        startLabel = Label(timeLabelFrame, text = "Beginning of times to ignore")
        startLabel.pack(anchor = "n", side = "left")
        endLabel = Label(timeLabelFrame, text = "End of times to ignore")
        endLabel.pack(anchor = "e", side = "top")

    def __ApplyIgnoreHoursState(self):
        if self.ignoreHoursStateVar.get() == "disabled":
            self.ignoreHoursInputFrame.pack_forget() 
        else: 
            self.ignoreHoursInputFrame.pack(side = "bottom", fill = "both", expand = 1, padx = 12, pady = 12)

    def GetStartTime(self):
        return time(
                    hour = int(self.ignoreStartHourVar.get()) + (12 if self.ignoreStartTimeOfDayVar.get() is "PM" else 0),
                    minute = int(self.ignoreStartMinuteVar.get())
                    )

    def GetEndTime(self):
        return time(
                    hour = int(self.ignoreEndHourVar.get()) + (12 if self.ignoreEndTimeOfDayVar.get() is "PM" else 0),
                    minute = int(self.ignoreEndMinuteVar.get())
                    )

    def GetIgnoreState(self):
        return True if self.ignoreHoursStateVar.get() is "normal" else False
