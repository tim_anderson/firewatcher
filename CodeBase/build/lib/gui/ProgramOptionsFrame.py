'''
Created on Jun 24, 2013

@author: tim.anderson

    Megawidget for the program features of FireWatcher.
    Use GetVideoFPS() to get the entered fps,
    GetCamFolder() to get the cam's folder, and
    GetSelectedFolder to get where the cam folder is located.

'''
from Tkinter import LabelFrame, Frame, Button, Scrollbar, Listbox, Label, StringVar, Radiobutton, Checkbutton
from tkFileDialog import askdirectory
from os import path, listdir
from tools.ConfigValues import DefaultValues
from Popups import createBasicPopup

class ProgramOptionsFrame(LabelFrame):
    def __init__(self, parent,  **options):
        LabelFrame.__init__(self, parent, **options)
        
        self.selectedFolder = None
        self.masterGui      = parent
        self.videoSpeed     = StringVar()
        self.timestampVar   = StringVar()
        self.timestampVar   .set(DefaultValues["UseTimestamps"])
   
        
        framerateFrame      = LabelFrame(self, text = "Video Speed")
        framerateFrame      .pack(side = "top", fill = "x", expand = 0, anchor = "n", pady = "12", padx = "8")
        slowRadioButton     = Radiobutton(framerateFrame, text = "slow", variable = self.videoSpeed, 
                                          value = "slow")
        slowRadioButton     .grid(row = 0, column = 0)
        normalRadioButton   = Radiobutton(framerateFrame, text = "normal", variable = self.videoSpeed, 
                                            value = "normal")
        normalRadioButton   .grid(row = 0, column = 1)
        fastRadioButton     = Radiobutton(framerateFrame, text = "fast", variable = self.videoSpeed, 
                                          value = "fast")
        fastRadioButton      .grid(row = 1, column = 0)
        veryfastRadioButton = Radiobutton(framerateFrame, text = "very fast", variable = self.videoSpeed, 
                                              value = "very fast")
        veryfastRadioButton .grid(row = 1, column = 1)
        timestampToggle     = Checkbutton(self, text = "Add Timestamps to video?", variable = self.timestampVar)
        timestampToggle     .pack(pady = 8)

        folderSelectorFrame = Frame(self)
        folderSelectorFrame .pack(side = "bottom", expand = 1, fill = "x", anchor = "s", padx = 8, pady = 20)
        selectorButton      = Button(folderSelectorFrame, command = self.__SelectFolder,
                                     text = "select a different\n directory")
        selectorButton      .pack(anchor = "n")
        self.selectorLabel  = Label(folderSelectorFrame, text = "no folder currently selected")
        self.selectorLabel  .pack(anchor = "n", pady = 12)
        scrollbar           = Scrollbar(folderSelectorFrame, orient = "vertical")
        self.camFolderList  = Listbox(folderSelectorFrame, yscrollcommand = scrollbar.set)
        scrollbar           .config(command = self.camFolderList.yview)
        scrollbar           .pack(side = "right", fill = "y")
        self.camFolderList  .pack(fill = "both", expand = 1, anchor = "s")
        
        self.videoSpeed     .set(DefaultValues["VideoSpeed"])
        self.__PopulateFolderOptions(DefaultValues["SelectedFolder"])

    def getVideoFPS(self):
        speed = self.videoSpeed.get()
        if speed == "slow":     return "5"
        elif speed == "normal": return "10"
        elif speed == "fast":   return "16"
        else:                   return "24"

    def getCamFolder(self):
        selection = self.camFolderList.curselection()
        return self.camFolderList.get(selection, selection)[0] if len(selection)>0 else None

    def getSelectedFolder(self):
        return self.selectedFolder

    def GetTimestampChoice(self):
        return self.timestampVar.get()

    def __SelectFolder(self):
        directory = askdirectory(parent = self, mustexist = True)
        if directory is not None and directory is not "":
            self.selectedFolder = directory
            self.__PopulateFolderOptions(directory, False)

    def __PopulateFolderOptions(self, directory, showPopupOnError = False):
        try:
            dirs = [ path.join(directory, x) for x in listdir(directory)
                    if path.isdir(path.join(directory, x)) and len(listdir(path.join(directory, x))) > 0]
            dirs.sort()
            self.masterGui.ShowOutput(
               ["Sucessfully found directory" + directory + ".",
                 "\t" + str(len(dirs)) + " subdirectories found."])
            self.selectorLabel.config(text = "Current Dir:\n" + directory)
            self.selectedFolder = directory
            self.camFolderList.delete(0, "end")
            for item in dirs:
                shortpath = path.basename(item)
                self.camFolderList.insert("end", shortpath)
        except Exception:
            if showPopupOnError:
                createBasicPopup("Found no directories with the name " + directory + ".", "No Directory Found")
            self.masterGui.ShowOutput(["WARNING: Directory was not found or lacks access permissions.",
                                      "Directory: "+ directory])
