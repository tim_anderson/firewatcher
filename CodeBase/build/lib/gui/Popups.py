'''
Created on Jun 14, 2013

@author: tim.anderson

    Module of popups for alerting the Gui.

'''
from Tkinter import Toplevel, Button, Label, Frame, StringVar, Entry
from tools.ConfigValues import ProgramTitle, CurrentVersion, outputDirName, licensePath, lesserLicencePath
from os import path
from webbrowser import open as openInBrowser
from subprocess import call

def newRegexPopup():
    top = Toplevel()
    top.title = "No matching Regex found."
    Label(top, text = "No matching regular expression was found for the given camera.\nPlease change the naming scheme of the images, or add a new regex to regexes.txt in the lib folder.").pack()
    Button(top, text = "Okay", command = top.destroy).pack()

def createBasicPopup(message, title):
    top = Toplevel()
    top.title = title
    Label(top, text = message).pack()
    Button(top, text = "Okay", command = top.destroy).pack()

def AboutScreen():
    popup = Toplevel()
    frame = Frame(popup)
    frame.pack(padx = 12, pady = 12)
    popup.title("About" + ProgramTitle + " v" + CurrentVersion)
    msg = Label(frame, text = ProgramTitle + "\nVersion " + CurrentVersion + "\n\n written by Tim Anderson for NTSG\n at the University of Montana.")
    msg.pack(pady = 8)
    Label(frame, text = "This software incorporates the FFMpeg executable.").pack(pady = 8)
    Button(frame, text = "Click here for more information about FFMpeg.",
                  command = lambda: openInBrowser("http://www.ffmpeg.org/index.html"), fg = "blue").pack(pady = 8)
    Button(frame, text = "This software is released under LGPL license, click here for more information.",
                   command = lambda :(openInBrowser(licensePath), openInBrowser(lesserLicencePath)), fg = "blue").pack(pady = 8)
    b = Button(frame, text = "okay", command = popup.destroy)
    b.pack(pady = 16)

# calls the onFinish function if the filename is unique, otherwise makes the user choose a unique name.
def nameChoicePopup(defaultName, onFinish):
    top = Toplevel()
    var = StringVar()
    var.set(defaultName)
    Label(top, text = "Please choose a name for the video to be created.").pack(padx = 10, pady = 10)
    Entry(top, textvariable = var, width = 25).pack(padx = 10, pady = 10)
    warningLabel = Label(top, text = "")
    warningLabel.pack()
    Button(top, text = "Okay", command =
                   lambda : nameChoiceHelper(onFinish, top, var.get(), warningLabel)
                   ).pack(side = "bottom", padx = 10, pady = 10)

def nameChoiceHelper(onFinish, popup, name, warningLabel):
    pathExists = path.exists(path.join(outputDirName, name))
    if pathExists:
        warningLabel.config(text = "a file already exists with this name.\nChoose a different name.")
    elif ":" in name:
        warningLabel.config(text = "Filename cannot contain a colon.\nPlease try a different name")
    else:
        popup.destroy()
        onFinish(name)
