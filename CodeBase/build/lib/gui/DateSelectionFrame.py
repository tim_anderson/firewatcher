'''
Created on Jun 24, 2013

@author: tim.anderson

    Megawidget containing a ttkcalendar for date selection, and optionMenus for selecting the run length.
    Use GetStartDate() to get the selected start dateTime.
    Use GetRunTime() to get the timedelta for the video runtime.

'''
from tools.ConfigValues import hoursList, minutesList, DefaultValues
from Tkinter import Frame, LabelFrame, OptionMenu, Label, StringVar
from ttkcalendar import Calendar
from ttk import Separator
from datetime import timedelta, datetime

class DateSelectionFrame(Frame):
    def __init__(self, parent, **options):
        Frame.__init__(self, parent, **options)
        
        self.startDate      = None
        self.startHour      = StringVar()
        self.startTimeOfDay = StringVar()
        self.daysToRun      = StringVar()
        self.hoursToRun     = StringVar()
        self.minutesToRun   = StringVar()
        self.startHour      .set(DefaultValues["StartHour"])
        self.startTimeOfDay .set(DefaultValues["StartTimeOfDay"])
        self.daysToRun      .set(DefaultValues["DaysToRun"])
        self.hoursToRun     .set(DefaultValues["HoursToRun"])
        self.minutesToRun   .set(DefaultValues["MinutesToRun"])
        
        dateStartFrame = LabelFrame(self, text = "Start Time and Date", padx = 30, pady = 18)
        dateStartFrame.grid(row = 0, column = 0, sticky = "W")
        captureLengthFrame = LabelFrame(self, text = "Capture Length", padx = 30, pady = 18)
        captureLengthFrame.grid(row = 0, column = 1, columnspan = 2, sticky = "news", padx = "0")
        self.startCalendar = Calendar(dateStartFrame)
        self.startCalendar.grid(row = 0, column = 0, columnspan = 2)
        startTime = OptionMenu(dateStartFrame, self.startHour, *hoursList)
        startTime.grid(row = 1, column = 0, sticky = "E")
        startTimeOfDaySelector = OptionMenu(dateStartFrame, self.startTimeOfDay, "AM", "PM")
        startTimeOfDaySelector.grid(row = 1, column = 1, sticky = "W")
        dayLabel = Label(captureLengthFrame, text = "Days")
        dayLabel.grid(row = 0, column = 0, columnspan = "3")
        runTimeDays = OptionMenu(captureLengthFrame, self.daysToRun, *range(0, 6))
        runTimeDays.grid(row = 1 , column = 0, columnspan = "3")
        spacer = Separator(captureLengthFrame, orient = "vertical")
        spacer.grid(row = 2, column = 0, columnspan = 3, pady = "12")
        timeLabel = Label(captureLengthFrame, text = "Hours/ Minutes")
        timeLabel.grid(row = 3, column = 0, columnspan = "3")
        runTimeHours = OptionMenu(captureLengthFrame, self.hoursToRun, *range(0, 25))
        runTimeHours.grid(row = 4, column = 0)
        seperator = Label(captureLengthFrame, text = ":")
        seperator.grid(row = 4, column = 1)
        runTimeMinutes = OptionMenu(captureLengthFrame, self.minutesToRun, *minutesList)
        runTimeMinutes.grid(row = 4, column = 2)

    # Generates a datetime object describing the selected start date and time.
    def getStartDateTime(self):
        calendarDate = self.startCalendar.selection
        return datetime(
                        year = calendarDate.year,
                        month = calendarDate.month,
                        day = calendarDate.day,
                        hour = int(self.startHour.get()) + (12 if self.startTimeOfDay is "PM" else 0)
                        ) if calendarDate is not None else None

    # Generates a timedelta object describing the selected runtime of the video.
    def getRunTime(self):
        return timedelta(
                         days       = int(self.daysToRun.get()),
                         hours      = int(self.hoursToRun.get()),
                         minutes    = int(self.minutesToRun.get()) 
                         )