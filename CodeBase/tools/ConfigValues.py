'''
Created on Jun 10, 2013

@author: tim.anderson
'''
from os import path
from sys import platform
from tempfile import gettempdir
import sys

here = path.dirname(sys.executable) if getattr(sys, "frozen", False) else path.dirname(__file__) 
capFramerate = True
minutesPerFrameCap = 3
defaultDirectory = r"\\secure\ftp\users\cams"
#optionsFileLocation = path.abspath(path.join(here, "options.dat"))
FFMpegLocationWin32 = path.abspath(path.join(here, "bin", "ffmpeg.exe"))
TimestamperLocation = path.abspath(path.join(here, "bin", "ImageTimestamper.exe"))
#FFMpegLocationiOS = path.abspath(path.join(here, "ffmpeg", "ffmpeg"))
#FFMpegLocationLinux = path.abspath(path.join(here, "ffmpeg", "ffmpeg.derp"))
outputDirName = path.expanduser('~')
tempDirectory = path.abspath(path.join(gettempdir(), "Firewatcher_Temp"))
regexPath = path.abspath(path.join(here, "config","regex.txt"))
licensePath = path.abspath(path.join(here, "docs","COPYING.txt"))
lesserLicencePath = path.abspath(path.join(here, "docs","COPYING.LESSER.txt"))
ProgramTitle = "Firewatcher"
CurrentVersion = "1.1.3 Beta"
ImagePattern = "Image%05d.jpeg"
fileExtention = ""
winEncoder = "libx264"  # Mp4 format. This is good for windows machines.
# encoder = "libxvid"   # Avi format.
# encoder = "libtheora"   Ogg format. Abandoned due to lack of support from common media players, despite high quality.
#macEncoder = "h264"  # Mov format, preferred on mac machines.
debugMode = False   #use this for any testing, so it can be turned off for mock deployment, without removing unit tests.

hoursList   = ["12"] + range(1, 12)
minutesList = (map(lambda x: "0" + str(x), range(0, 10)) + (range(10, 60)))

if platform == "win32":
    FFMpegLocation = FFMpegLocationWin32
    encoder = winEncoder
    fileExtention = ".mp4"
#elif platform == "darwin":
#    FFMpegLocation = FFMpegLocationiOS
#    encoder = macEncoder
#    fileExtention = ".mov"

def CreateDefaultValues():
    DefaultValues = {}
    DefaultValues["IgnoreState"] = "disabled"
    DefaultValues["IgnoreStartMinute"] = "00"
    DefaultValues["IgnoreEndMinute"] = "00"
    DefaultValues["IgnoreStartHour"] = "10"
    DefaultValues["IgnoreEndHour"] = "6"
    DefaultValues["IgnoreStartTimeOfDay"] = "PM"
    DefaultValues["IgnoreEndTimeOfDay"] = "AM"
    DefaultValues["VideoSpeed"] = "normal"
    DefaultValues["StartHour"] = "8"
    DefaultValues["StartTimeOfDay"] = "AM"
    DefaultValues["DaysToRun"] = "0"
    DefaultValues["HoursToRun"] = "0"
    DefaultValues["MinutesToRun"] = "00"
    DefaultValues["SelectedFolder"] = defaultDirectory
    DefaultValues["UseTimestamps"] = "0"
    return DefaultValues

DefaultValues = CreateDefaultValues()
