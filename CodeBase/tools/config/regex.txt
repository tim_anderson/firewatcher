*****Each line has the regular expression, followed by indecies for the hour and minute values*******
*************Negative values for indecies reference from the back of the string**********************
*****Example, -1 means the last letter in the string, while 3 means the 4th letter in the string*****
^[0-9]{9}$ -9 -7 -7 -5
^[a-zA-Z]+[-_][0-9]{6}[-_][0-9]{6}$ -6 -4 -4 -2
^[a-zA-Z]+[_-][0-9]{8}[a-zA-Z][0-9]{9}$ -9 -7 -7 -5