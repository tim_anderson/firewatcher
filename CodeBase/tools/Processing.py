'''
Created on Jun 14, 2013
 
@author: tim.anderson
'''
from PIL.Image import open as imgOpen, new as newImage
from PIL.ImageFont import load_default as fontLoad
from PIL.ImageDraw import Draw as DrawOn
from PIL.ImageOps import invert as InvertColors
import datetime
from os.path import exists
import JpegImagePlugin
import Image
Image._initialized = 2

textScale = 2
font = fontLoad()

def Resize (imageSrc, newWidth, newHeight):
    with imgOpen(imageSrc, "r") as oldImage:
        newImage = oldImage.resize((newWidth, newHeight))
        return newImage

# Creates and returns a timestamp for the given datetime. returns a 0 height, 0 width img on error.
def CreateTimestamp(datetime):
    try:
        text = "".join([str(datetime.month), "/", str(datetime.day), "/", str(datetime.year), " ", str(datetime.hour), ":", str(datetime.minute)])
        fontSize = font.getsize(text)
        # Create the text image.
        fontImage = newImage("RGB", (fontSize[0], fontSize[1]), "black")
        DrawOn(fontImage).text((0, 0), text, font = font)
        # multiply the text scale by the text scale.
        fontImage = fontImage.resize((fontSize[0] * textScale, fontSize[1] * textScale))
        fontImage = InvertColors(fontImage)     #flips the colors for white text on black background.
        return fontImage
    except:
        return None
