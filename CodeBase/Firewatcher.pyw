'''
Created on Jun 3, 2013

@author: tim.anderson

    Runnable, windowless entrypoint for the Firewatcher gui application
    This module with throw an error if it can't find the lib folder.

'''

from tools.ConfigValues import ProgramTitle, CurrentVersion
from gui.Gui import Gui

def Main():
    root = Gui()
    root.wm_title(" ".join([ProgramTitle, CurrentVersion]))
    root.mainloop()
if __name__ == '__main__':
    Main()
