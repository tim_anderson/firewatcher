'''
Created on Jul 3, 2013
 
@author: tim.anderson
'''
from tools.ConfigValues import ProgramTitle, CurrentVersion, regexPath, licensePath, lesserLicencePath, FFMpegLocation
from os.path import join
import sys
from distutils.core import setup
import py2exe
base = None
if sys.platform == "win32":
    base = "Win32GUI"
 
setup(
    windows = ["Firewatcher.pyw"],
    name = ProgramTitle,
    version = CurrentVersion,
    description = "Time-lapse capturing over large timeframes.",
    author = "Tim Anderson",
    author_email = "TimAnderson.UM@Gmail.com",
    packages = ["tools", "gui"],
    url = r'https://bitbucket.org/tim_anderson/firewatcher/',
    py_modules = ["Firewatcher"],
#     "PIL.ImageFont","PIL.ImageDraw","PIL.ImageOps","datetime","tempfile","os","sys","subprocess",""
     data_files = [
                   ('docs', [lesserLicencePath, licensePath]),
                   ('config', [regexPath]),
                   ('bin', [FFMpegLocation])
                   ]

      )